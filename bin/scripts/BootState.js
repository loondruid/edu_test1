/// <reference path="phaser/phaser.d.ts"/>
/// <reference path="Game.ts"/>
var Candy;
(function (Candy) {
    class BootState {
        constructor(game) {
            this.game = game;
        }
        preload() {
            this.game.load.image('preloaderBar', 'assets/images/loading-bar.png');
        }
        create() {
            let game = this.game;
            //configure view port:
            game.input.maxPointers = 1;
            game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            game.scale.pageAlignHorizontally = true;
            game.scale.pageAlignVertically = true;
            //game.scale.updateLayout();
            //start preloader
            game.state.start('PreloaderState');
        }
    }
    Candy.BootState = BootState;
})(Candy || (Candy = {}));
