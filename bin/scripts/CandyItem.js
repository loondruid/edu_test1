/// <reference path="Player.ts" />
/// <reference path="Config.ts" />
/// <reference path="Game.ts" />
/// <reference path="phaser/phaser.d.ts"/>
var Candy;
(function (Candy) {
    class CandyItem {
        constructor(game) {
            this.alive = 1;
            this.game = game;
            let dropPos = Math.floor(Math.random() * Candy.GAME_WIDTH);
            let dropOffset = [100, 100, 100, 100, 100];
            let candyType = Math.floor(Math.random() * 5);
            this.candy = game.add.sprite(dropPos, dropOffset[candyType], 'candy');
            let candy = this.candy;
            candy.animations.add('anim', [candyType], 10, true);
            candy.animations.play('anim');
            game.physics.enable(candy, Phaser.Physics.ARCADE);
            candy.inputEnabled = true;
            candy.events.onInputDown.add(this.clickCandy, this.candy);
            candy.checkWorldBounds = true;
            candy.events.onOutOfBounds.add(this.removeCandy, this.candy);
            candy.anchor.setTo(0.5, 0.5);
            this.rotateMe = (Math.random() * 4) - 2;
            game.candyGroup.add(candy);
        }
        clickCandy(candy) {
            candy.kill();
            this.game.score += 1;
            this.game.scoreText.setText('' + this.game.score);
            this.alive = 0;
        }
        removeCandy(candy) {
            candy.kill();
            this.game.player.health -= 10;
            this.alive = 0;
        }
    }
    Candy.CandyItem = CandyItem;
})(Candy || (Candy = {}));
