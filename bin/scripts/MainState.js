/// <reference path="Config.ts" />
/// <reference path="phaser/phaser.d.ts" />
/// <reference path="Game.ts" />
/// <reference path="CandyItem.ts" />
var Candy;
(function (Candy) {
    class MainState extends Phaser.State {
        constructor(game) {
            super();
            this.game = game;
            this.candies = [];
        }
        create() {
            let game = this.game;
            //initiating Game object
            game.score = 0;
            game.spawnCandyTimer = 0;
            game.fontStyle = { font: "40px Arial", fill: "#FFCC00", stroke: "#333", strokeThickness: 5, align: "center" };
            //start and config physics
            game.physics.startSystem(Phaser.Physics.ARCADE);
            game.physics.arcade.gravity.y = 200;
            //load assets
            game.add.sprite(0, 0, 'background');
            game.add.sprite(-30, Candy.GAME_HEIGHT - 160, 'floor');
            game.add.sprite(10, 5, 'score-bg');
            game.add.button(Candy.GAME_WIDTH - 96 - 10, 5, 'button-pause', function () { game.managePause(); });
            game.player = game.add.sprite(5, 760, 'monster-idle');
            game.player.health = 20;
            let player = game.player;
            player.animations.add('idle', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], 10, true);
            player.animations.play('idle');
            game.candyGroup = game.add.group();
            this.candies.push(new Candy.CandyItem(game));
            game.scoreText = game.add.text(120, 20, "0", game.fontStyle);
        }
        update() {
            let game = this.game;
            game.spawnCandyTimer += this.time.elapsed;
            if (game.spawnCandyTimer > 1000) {
                game.spawnCandyTimer = 0;
                this.candies.push(new Candy.CandyItem(game));
            }
            for (let i = 0; i < this.candies.length; i++) {
                //todo: remove dead candies
                if (this.candies[i].alive != 0)
                    this.candies[i].candy.angle += this.candies[i].rotateMe;
            }
            if (!game.player.health) {
                game.add.sprite((Candy.GAME_WIDTH - 594) / 2, (Candy.GAME_HEIGHT - 271) / 2, 'game-over');
                game.paused = true;
            }
        }
    }
    Candy.MainState = MainState;
})(Candy || (Candy = {}));
