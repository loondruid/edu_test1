/// <reference path="phaser/phaser.d.ts"/>
/// <reference path="Game.ts" />
/// <reference path="Config.ts" />
/// <reference path="BootState.ts" />
/// <reference path="PreloaderState.ts" />
/// <reference path="MainMenuState.ts" />
/// <reference path="MainState.ts" />
window.onload = function () {
    let game = new Candy.Game(Candy.GAME_WIDTH, Candy.GAME_HEIGHT, Phaser.AUTO, 'game');
    game.state.add('BootState', Candy.BootState, true);
    game.state.add('PreloaderState', Candy.PreloaderState, true);
    game.state.add('MainMenuState', Candy.MainMenuState, true);
    game.state.add('MainState', Candy.MainState, true);
    game.state.start('BootState');
};
