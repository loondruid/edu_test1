/// <reference path="Player.ts" />
/// <reference path="Config.ts" />
/// <reference path="Game.ts" />
/// <reference path="phaser/phaser.d.ts"/>
var Candy;
(function (Candy_1) {
    var Candy = (function () {
        function Candy(game) {
            this.game = game;
            var dropPos = Math.floor(Math.random() * Candy_1.GAME_WIDTH);
            var dropOffset = [100, 100, 100, 100, 100];
            var candyType = Math.floor(Math.random() * 5);
            this.candy = game.add.sprite(dropPos, dropOffset[candyType], 'candy');
            var candy = this.candy;
            candy.animations.add('anim', [candyType], 10, true);
            candy.animations.play('anim');
            game.physics.enable(candy, Phaser.Physics.ARCADE);
            candy.inputEnabled = true;
            candy.events.onInputDown.add(this.clickCandy, this);
            candy.checkWorldBounds = true;
            candy.events.onOutOfBounds.add(this.removeCandy, this);
            candy.anchor.setTo(0.5, 0.5);
            this.rotateMe = (Math.random() * 4) - 2;
            game.candyGroup.add(candy);
        }
        Candy.prototype.clickCandy = function (candy) {
            candy.kill();
            this.game.score += 1;
            this.game.scoreText.setText('Score: ' + this.game.score);
        };
        Candy.prototype.removeCandy = function (candy) {
            candy.kill();
            this.game.player.health -= 10;
        };
        return Candy;
    }());
    Candy_1.Candy = Candy;
})(Candy || (Candy = {}));
