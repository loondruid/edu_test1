/// <reference path="phaser/phaser.d.ts" />
/// <reference path="FontStyle.ts"/>
/// <reference path="Player.ts" />
var Candy;
(function (Candy) {
    class Game extends Phaser.Game {
        constructor(xsize, ysize, renderType, name) {
            super(xsize, ysize, renderType, name);
        }
        managePause() {
            this.paused = true;
            var pausedText = this.add.text(100, 250, "Game paused.\nTap anywhere to continue.", this.fontStyle);
            this.input.onDown.add(function () {
                pausedText.destroy();
                this.paused = false;
            }, this);
        }
    }
    Candy.Game = Game;
})(Candy || (Candy = {}));
