/// <reference path="Config.ts" />
/// <reference path="Game.ts" />
var Candy;
(function (Candy) {
    class MainMenuState {
        constructor(game) {
            console.log("creating hame " + game);
            this.game = game;
        }
        create() {
            let game = this.game;
            game.add.sprite(0, 0, 'background');
            game.add.sprite(-130, Candy.GAME_HEIGHT - 514, 'monster-cover');
            game.add.sprite((Candy.GAME_WIDTH - 395) / 2, 60, 'title');
            let self = this;
            game.add.button(Candy.GAME_WIDTH - 401 - 10, Candy.GAME_HEIGHT - 143 - 10, 'button-start', function () { self.startGame(); }, game, 1, 0, 2);
        }
        startGame() {
            this.game.state.start('MainState');
        }
    }
    Candy.MainMenuState = MainMenuState;
})(Candy || (Candy = {}));
