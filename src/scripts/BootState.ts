/// <reference path="phaser/phaser.d.ts"/>
/// <reference path="Game.ts"/>

namespace Candy {
    export class BootState {
        game: Candy.Game;

        constructor (game: Candy.Game) {
            this.game = game;
        }

        preload() {
            this.game.load.image('preloaderBar', 'assets/images/loading-bar.png');
        }

        create() {
            let game = this.game;

            //configure view port:
            game.input.maxPointers = 1;
            game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            game.scale.pageAlignHorizontally = true;
            game.scale.pageAlignVertically = true;
            //game.scale.updateLayout();

            //start preloader
            game.state.start('PreloaderState');
        }
    }
}
