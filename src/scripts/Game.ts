/// <reference path="phaser/phaser.d.ts" />
/// <reference path="FontStyle.ts"/>
/// <reference path="Player.ts" />

namespace Candy {
    export class Game extends Phaser.Game {
        preloadBar: Phaser.Sprite;
        player: Candy.Player;
        candyGroup: Phaser.Group;
        spawnCandyTimer: number;
        fontStyle: FontStyle;
        scoreText: Phaser.Text;
        score: number;

        constructor(xsize, ysize, renderType, name) {
            super(xsize, ysize, renderType, name);
        }

        managePause() {
            this.paused = true;
            var pausedText = this.add.text(100, 250, "Game paused.\nTap anywhere to continue.", this.fontStyle);
            this.input.onDown.add(function(){
                pausedText.destroy();
                this.paused = false;
            }, this);
        }    
    }
}