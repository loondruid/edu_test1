/// <reference path="Config.ts"/>
/// <reference path="Game.ts"/>

namespace Candy {
    export class PreloaderState {
        game: Candy.Game;

        constructor(game: Candy.Game) {
            this.game = game;
        }

        preload() {
            let game = this.game; //alias
            
            game.stage.backgroundColor = '#B4D9E7';

            //show preload bar before loading
            game.preloadBar = game.add.sprite((Candy.GAME_WIDTH-311)/2, (Candy.GAME_HEIGHT-27)/2, 'preloaderBar');
            game.load.setPreloadSprite(game.preloadBar);


            //load assets
            game.load.image('background', 'assets/images/background.png');
            game.load.image('floor', 'assets/images/floor.png');
            game.load.image('monster-cover', 'assets/images/monster-cover.png');
            game.load.image('title', 'assets/images/title.png');
            game.load.image('game-over', 'assets/images/gameover.png');
            game.load.image('score-bg', 'assets/images/score-bg.png');
            game.load.image('button-pause', 'assets/images/button-pause.png');
            game.load.spritesheet('candy', 'assets/images/candy.png', 82, 98);
            game.load.spritesheet('monster-idle', 'assets/images/monster-idle.png', 103, 131);
            game.load.spritesheet('button-start', 'assets/images/button-start.png', 401, 143);

        }

        create() {
            this.game.state.start('MainMenuState');
        }
    }
}
