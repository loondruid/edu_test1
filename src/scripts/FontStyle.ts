class FontStyle {
    font: string;
    fill: string;
    stroke: string;
    strokeThickness: number;
    align: string;
}